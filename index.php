<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/24
 * Time: 16:15
 */
require_once __DIR__."/vendor/autoload.php";
function dump($vars, $label = '', $return = false) {
    if (ini_get('html_errors')) {
        $content = "<pre>\n";
        if ($label != '') {
            $content .= "<strong>{$label} :</strong>\n";
        }
        $content .= htmlspecialchars(print_r($vars, true));
        $content .= "\n</pre>\n";
    } else {
        $content = $label . " :\n" . print_r($vars, true);
    }
    if ($return) { return $content; }
    echo $content;
    return null;
}

$arr=[
    ["id" => 1,"parent_id"=>0,"sort"=>2,"title"=>"第1层数据"],
    ["id" => 2,"parent_id"=>0,"sort"=>1,"title"=>"第2层数据"],
    ["id" => 3,"parent_id"=>1,"sort"=>1,"title"=>"第3层数据"],
    ["id" => 4,"parent_id"=>1,"sort"=>2,"title"=>"第4层数据"],
    ["id" => 5,"parent_id"=>2,"sort"=>1,"title"=>"第5层数据"],
    ["id" => 6,"parent_id"=>1,"sort"=>4,"title"=>"第3层数据"],
    ["id" => 7,"parent_id"=>1,"sort"=>3,"title"=>"第4层数据"],
];
$tree=new \suger\tree\Tree($arr);
$tree->getTree();